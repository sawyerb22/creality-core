"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.compact = function () {
    var elements = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        elements[_i] = arguments[_i];
    }
    return elements.filter(function (x) { return x; });
};
exports.default = exports.compact;
