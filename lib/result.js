"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable-next-line:no-any
exports.isValueOrError = function (arg) {
    return arg && ((arg.value && !arg.error) || (!arg.value && arg.error));
};
var RESULT = 'RESULT';
exports.Result = function (valueOrError) {
    return valueOrError instanceof Error
        ? failureResult(valueOrError)
        : successResult(valueOrError);
};
var successResult = function (value) { return ({
    value: value,
    type: RESULT,
    map: function (transform) {
        try {
            return successResult(transform(value));
        }
        catch (error) {
            return failureResult(error);
        }
    },
    match: function (success, failure) {
        try {
            return success(value);
        }
        catch (error) {
            return failure(error);
        }
    },
    get: function (key) { return value[key]; },
    select: function (key) {
        var selected = value[key];
        return selected === null
            ? failureResult(Error(key + " is null on " + value))
            : selected === undefined
                ? failureResult(Error(key + " is undefined on " + value))
                : successResult(selected);
    },
}); };
var failureResult = function (error) { return ({
    error: error,
    type: RESULT,
    map: function () { return failureResult(error); },
    match: function (_, failure) {
        return failure(error);
    },
    get: function () { return undefined; },
    select: function (key) { return failureResult(error); },
}); };
exports.zipResults = function (results) {
    return exports.Result(Object.keys(results).reduce(function (valueOrError, key) {
        var _a;
        return valueOrError instanceof Error
            ? valueOrError
            : results[key].value
                ? __assign({}, valueOrError, (_a = {}, _a[key] = results[key].value, _a)) : results[key].error;
    }, {}));
};
// tslint:disable-next-line:no-any
exports.isResult = function (arg) {
    return exports.isValueOrError(arg) && arg.type === RESULT;
};
exports.default = exports.Result;
