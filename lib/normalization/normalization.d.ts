import { Action as ReduxAction } from 'redux';
declare type NormalizedDataType = {
    [typename: string]: {
        [id: string]: any;
    };
};
export declare type Normalizable<NormalizedData extends NormalizedDataType, Key extends keyof NormalizedData> = {
    __typename: Key;
    id: string;
};
declare type KeyOf<T> = T extends Normalizable<any, infer Key> ? Key : never;
export declare type Normalized<NormalizedData extends NormalizedDataType, T extends Normalizable<NormalizedData, any>> = {
    __typename: KeyOf<T>;
    id: string;
    isNormalized: true;
};
export declare const createNormalize: <NormalizedData extends NormalizedDataType>() => <T extends Normalizable<NormalizedData, any>>(value: T) => Normalized<NormalizedData, T>;
declare type NormalizedDataReducers<NormalizedData, Action> = {
    [K in keyof NormalizedData]?: ((data: NormalizedData[K], action: Action) => NormalizedData[K]);
};
export declare const createReducer: <NormalizedData extends NormalizedDataType, Action extends ReduxAction<any>>(normalizedData: NormalizedData, reducers?: NormalizedDataReducers<NormalizedData, Action>) => (data: NormalizedData | undefined, action: Action) => NormalizedData;
declare type ById<T> = {
    [id: string]: T | undefined;
};
export declare const updateData: <T>(data: ById<T>, id: string, update: (value: T) => Partial<T>) => ById<T>;
export declare const createDenormalize: <NormalizedData extends NormalizedDataType>() => <T extends Normalizable<any, any>>(data: NormalizedData) => (normalized: Normalized<NormalizedData, T>) => T;
export {};
