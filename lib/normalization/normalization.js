"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
/* tslint:disable:no-any */
var deepmerge_1 = require("deepmerge");
var lodash_mapvalues_1 = require("lodash.mapvalues");
var lodash_pickby_1 = require("lodash.pickby");
var util_1 = require("util");
var deepmergeOptions = {
    arrayMerge: function (destination, source) { return source; },
};
var normalizeObject = function (object) {
    return lodash_mapvalues_1.default(object, normalizeValue);
};
var normalizeValue = function (value) {
    if (typeof value != 'object' || value == null) {
        return value;
    }
    else if (util_1.isArray(value)) {
        return value.map(normalizeValue);
    }
    else if (isNormalizable(value)) {
        return exports.createNormalize()(value);
    }
    else {
        return normalizeObject(value);
    }
};
var denormalizeObject = function (object, data) { return lodash_mapvalues_1.default(object, function (value) { return denormalizeValue(value, data); }); };
var denormalizeValue = function (value, data) {
    if (typeof value != 'object' || value == null) {
        return value;
    }
    else if (util_1.isArray(value)) {
        return value.map(function (x) { return denormalizeValue(x, data); });
    }
    else if (isNormalized(value)) {
        return exports.createDenormalize()(data)(value);
    }
    else {
        return denormalizeObject(value, data);
    }
};
var isNormalizable = function (value) {
    return value && typeof value.__typename == 'string' && typeof value.id == 'string';
};
var isNormalized = function (value) {
    return value &&
        typeof value.__typename == 'string' &&
        typeof value.id == 'string' &&
        value.isNormalized == true;
};
var gatherNormalizedData = function (value) {
    var _a, _b;
    if (typeof value != 'object' || value == null) {
        return {};
    }
    else if (util_1.isArray(value)) {
        return deepmerge_1.all(value.map(gatherNormalizedData), deepmergeOptions);
    }
    else {
        return deepmerge_1.all([
            isNormalizable(value)
                ? (_a = {}, _a[value.__typename] = (_b = {}, _b[value.id] = normalizeObject(value), _b), _a) : {}
        ].concat(Object.values(value).map(gatherNormalizedData)), deepmergeOptions);
    }
};
exports.createNormalize = function () { return function (value) { return ({
    __typename: value.__typename,
    id: value.id,
    isNormalized: true,
}); }; };
exports.createReducer = function (normalizedData, reducers) {
    if (reducers === void 0) { reducers = {}; }
    return function (data, action) {
        if (data === void 0) { data = normalizedData; }
        var newData = gatherNormalizedData(action);
        return Object.keys(data).reduce(function (data, k) {
            var _a;
            var key = k;
            var reducer = reducers[key];
            var nextSubset = reducer
                ? reducer(data[key], action)
                : data[key];
            return nextSubset !== data[key] ? __assign({}, data, (_a = {}, _a[key] = nextSubset, _a)) : data;
        }, Object.keys(newData).length > 0
            ? deepmerge_1.default(data, newData, deepmergeOptions)
            : data);
    };
};
exports.updateData = function (data, id, update) {
    var _a;
    var value = data[id];
    return value
        ? __assign({}, data, (_a = {}, _a[id] = deepmerge_1.default(value, lodash_pickby_1.default(update(value), function (value) { return value != undefined; }), deepmergeOptions), _a)) : data;
};
exports.createDenormalize = function () { return function (data) { return function (normalized) {
    return denormalizeObject(data[normalized.__typename][normalized.id], data);
}; }; };
