export declare const compact: <T>(...elements: (false | T | null | undefined)[]) => T[];
export default compact;
