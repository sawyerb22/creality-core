export declare type ValueOrError<T> = {
    type: typeof RESULT;
    value: T;
    error?: undefined;
} | {
    type: typeof RESULT;
    value?: undefined;
    error: Error;
};
export declare const isValueOrError: <T>(arg: any) => arg is ValueOrError<T>;
declare const RESULT = "RESULT";
export declare type NotNullOrUndefined<T> = Exclude<T, undefined | null>;
export declare type Result<T> = ValueOrError<T> & {
    type: typeof RESULT;
    map<U>(transform: (value: T) => U): Result<U>;
    match<U>(success: (value: T) => U, failure: (error: Error) => U): U;
    get<K extends keyof T>(key: K): T[K] | undefined;
    select<K extends keyof T>(key: K): Result<NotNullOrUndefined<T[K]>>;
};
export declare const Result: <T>(valueOrError: Error | T) => Result<T>;
export declare const zipResults: <V>(results: { [K in keyof V]: Result<V[K]>; }) => Result<V>;
export declare const isResult: <T>(arg: any) => arg is Result<T>;
export default Result;
