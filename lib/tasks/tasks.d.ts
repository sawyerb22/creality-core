import { Action as ReduxAction, Store as ReduxStore } from 'redux';
import { Result } from '../result';
export declare type Operation<Data> = {
    query: string;
    variables?: object;
};
export declare type Task<Action extends ReduxAction> = {
    operation: Operation<object>;
    completion: (result: Result<any>) => Action;
};
export declare const TaskConstructor: <Action extends ReduxAction<any>>() => <Data>(operation: Operation<Data>, completion: (result: Result<Data>) => Action) => Task<Action>;
export declare const updateTasks: <State, Action extends ReduxAction<any>>(store: ReduxStore<State, Action>, getTasks: (state: State) => Task<Action>[], maximumConcurrentTasks: number, enableLogging?: boolean | undefined) => void;
