"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var result_1 = require("../result");
var api_1 = require("./api");
exports.TaskConstructor = function () { return function (operation, completion) { return ({
    operation: operation,
    completion: completion,
}); }; };
var operationName = function (operationString) {
    var regex = /(query|mutation) (\w+)/.exec(operationString);
    return regex ? regex[1] + " " + regex[2] : '';
};
var KeyedTask = function (_a) {
    var operation = _a.operation, completion = _a.completion;
    return ({
        name: operationName(operation.query),
        key: JSON.stringify(operation),
        start: function (dispatch) {
            var cancelToken = axios_1.default.CancelToken.source();
            api_1.makeApiRequest(axios_1.default.post('/graphql', operation, {
                cancelToken: cancelToken.token,
            }), function (data) { return dispatch(completion(result_1.Result(data))); }, function (error) { return dispatch(completion(result_1.Result(error))); });
            return cancelToken;
        },
    });
};
var mutableRunningTasks = {};
var logTasks = function (tasks) {
    console.clear();
    tasks.forEach(function (task) { return console.log(task.name); });
};
exports.updateTasks = function (store, getTasks, maximumConcurrentTasks, enableLogging) {
    var keys = new Set();
    var tasks = getTasks(store.getState())
        .map(function (task) { return KeyedTask(task); })
        .filter(function (task) {
        // filter out duplicate tasks
        if (keys.has(task.key)) {
            return false;
        }
        else {
            keys.add(task.key);
            return true;
        }
    })
        .slice(0, maximumConcurrentTasks); // only keep up to our max
    if (enableLogging) {
        logTasks(tasks);
    }
    // Remove tasks no longer running
    Object.keys(mutableRunningTasks).forEach(function (key) {
        if (!keys.has(key)) {
            var cancelToken = mutableRunningTasks[key];
            cancelToken.cancel();
            delete mutableRunningTasks[key];
        }
    });
    // Start tasks not yet running
    tasks.forEach(function (task) {
        if (!(task.key in mutableRunningTasks)) {
            mutableRunningTasks[task.key] = task.start(store.dispatch);
        }
    });
};
