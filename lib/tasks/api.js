"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
exports.makeApiRequest = function (promise, 
// tslint:disable-next-line:no-any
handleSuccess, handleError) {
    return promise
        .then(function (response) {
        var data = response && response.data;
        if (data && data.errors) {
            handleError(Error('API Error: ' + data.errors[0].message));
            return;
        }
        return handleSuccess(data && data.data ? data.data : data);
    })
        .catch(function (error) {
        if (error && error.errors) {
            handleError(Error('Request Error: ' + error.errors[0].message));
        }
        else if (error && error.message === 'Network Error') {
            handleError(error);
        }
        else if (axios_1.default.isCancel(error)) {
            return;
        }
        else if (error) {
            handleError(error);
        }
    });
};
