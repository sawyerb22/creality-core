import { AxiosPromise } from 'axios';
export declare const makeApiRequest: (promise: AxiosPromise<any>, handleSuccess: (data: any) => void, handleError: (error: Error) => void) => Promise<void>;
