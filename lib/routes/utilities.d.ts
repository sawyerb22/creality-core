import { Route, RouteMeta } from './Route';
export declare type RouteType = {
    path: string;
    subroute?: RouteType;
};
export declare const findRoute: <Meta extends RouteMeta<any, any, any, any, any>>(route: RouteType, subroute: Meta) => Route<Meta> | undefined;
