"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var lodash_get_1 = require("lodash.get");
var lodash_zip_1 = require("lodash.zip");
var compact_1 = require("../compact");
var subrouteOf = function (route) {
    return lodash_get_1.default(route, 'subroute');
};
var composedFromPath = function (pattern, fromPath, subroute, subrouteIsRequired) { return function (path, location) {
    var result = fromPath(path, location);
    if (!result) {
        return undefined;
    }
    var partialRouteWithoutPath = result[0], remainingPath = result[1];
    var partialRoute = Object.assign({}, partialRouteWithoutPath, {
        path: pattern,
    });
    if (subroute) {
        var possibleSubroute = subroute.fromPath(remainingPath, location);
        if (possibleSubroute) {
            return Object.assign({}, partialRoute, {
                subroute: possibleSubroute,
            });
        }
    }
    return subrouteIsRequired && !lodash_get_1.default(partialRoute, 'subroute')
        ? undefined
        : partialRoute;
}; };
var components = function (path) {
    return path.split('/').filter(function (component) { return component != ''; });
};
var pathFromComponents = function (components) { return "/" + components.join('/'); };
var remainingPath = function (path, pattern) {
    return pathFromComponents(path.slice(pattern.length));
};
var variables = function (path, pattern) {
    return lodash_zip_1.default(path, pattern).reduce(function (variables, _a) {
        var path = _a[0], pattern = _a[1];
        var _b;
        return variables && pattern
            ? pattern.startsWith(':')
                ? __assign({}, variables, (_b = {}, _b[pattern.slice(1)] = path, _b)) : pattern === path
                ? variables
                : undefined
            : variables;
    }, {});
};
var variablesAndRemainingPath = function (path, pattern) { return [
    variables(path, pattern),
    remainingPath(path, pattern),
]; };
var fromPathWithPattern = function (pattern, init) { return function (path, location) {
    var _a = pattern.split('?'), pathPattern = _a[0], searchPattern = _a[1];
    var _b = variablesAndRemainingPath(components(path), components(pathPattern)), variables = _b[0], remainingPath = _b[1];
    var result = variables && init(variables);
    return result ? [result, remainingPath] : undefined;
}; };
var toPathComponentsWithPattern = function (pattern) { return function (route) {
    return compact_1.compact.apply(void 0, components(pattern).map(function (component) {
        return component.startsWith(':') ? lodash_get_1.default(route, component.slice(1)) : component;
    }));
}; };
var composedPath = function (pathComponents, subrouteMeta) { return function (route) {
    return pathFromComponents(pathComponents(route).concat(components(ifBoth(route, subrouteMeta, function (meta, subroute) { return meta.toPath(subroute); }) ||
        '')));
}; };
var composedTasks = function (tasks, subrouteMeta) { return function (route, state) { return (ifBoth(route, subrouteMeta, function (meta, subroute) {
    return meta.tasks(subroute, state);
}) || []).concat(tasks(route, state)); }; };
var composedReducer = function (path, reducer, subrouteMeta, subrouteIsRequired) { return function (route, action, state) {
    var newRoute = reducer(route, action, state);
    if (newRoute === undefined) {
        return undefined;
    }
    var subroute = subrouteOf(newRoute);
    if (subrouteMeta && subroute) {
        var newSubroute = subrouteMeta.reducer(subroute, action, state);
        if (newSubroute !== subroute) {
            return newSubroute === undefined && subrouteIsRequired
                ? undefined
                : Object.assign({}, newRoute, { subroute: newSubroute }, { path: path });
        }
    }
    return lodash_get_1.default(newRoute, 'path') === path
        ? newRoute
        : Object.assign({}, newRoute, { path: path });
}; };
var AccessLevel;
(function (AccessLevel) {
    AccessLevel[AccessLevel["PUBLIC"] = 0] = "PUBLIC";
    AccessLevel[AccessLevel["LOGGED_IN"] = 1] = "LOGGED_IN";
    AccessLevel[AccessLevel["PRIVATE"] = 2] = "PRIVATE";
})(AccessLevel = exports.AccessLevel || (exports.AccessLevel = {}));
var composedAccessLevel = function (accessLevel, subrouteMeta) { return function (route) {
    return Math.max(accessLevel, ifBoth(route, subrouteMeta, function (meta, subroute) {
        return meta.accessLevel(subroute);
    }) || 0);
}; };
var ifBoth = function (route, subrouteMeta, withSubroute) {
    var subroute = subrouteOf(route);
    return subrouteMeta && subroute
        ? withSubroute(subrouteMeta, subroute)
        : undefined;
};
var composedPredicate = function (value, predicate, subrouteMeta) { return function (route) {
    return value ||
        ifBoth(route, subrouteMeta, function (meta, subroute) { return meta[predicate](subroute); }) ||
        false;
}; };
exports.RouteConstructor = function () {
    /**
     * Creates a Route definition
     * @param path
     * REQUIRED: A pattern like `'/contacts/:id'`
     * @param init
     * A initializer that returns the route body
     * @param fromPath
     * An initializer that returns the route body given matching path variables
     * @param subroute
     * The Subroute definition if the route body contains a subroute
     * @param subrouteIsRequired
     * Must be marked true if `subroute` is not optional
     * @param tasks
     * A method that returns the tasks for this route
     * @param reducer
     * A reducer for this route
     * @param accessLevel
     * The access level for this route
     * @param hideSidebar
     * Whether to hide the sidebar when viewing this route.
     * @param hideTopBar
     * Whether to hide the top bar when viewing this route.
     * @param doNotResetWindowScroll
     * Mark `true` if the window scroll should not reset when navigating to or from this route.
     */
    return function (_a) {
        var path = _a.path, init = _a.init, fromPath = _a.fromPath, subroute = _a.subroute, subrouteIsRequired = _a.subrouteIsRequired, tasks = _a.tasks, reducer = _a.reducer, accessLevel = _a.accessLevel, hideSidebar = _a.hideSidebar, hideTopBar = _a.hideTopBar, doNotResetWindowScroll = _a.doNotResetWindowScroll;
        return Object.assign(function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            return Object.assign({}, init ? init.apply(void 0, args) : args[0] || {}, { path: path });
        }, {
            path: path,
            fromPath: composedFromPath(path, fromPathWithPattern(path, fromPath || (function () { return ({}); })), subroute, subrouteIsRequired),
            toPath: composedPath(toPathComponentsWithPattern(path), subroute),
            subroute: subroute,
            subrouteIsRequired: subrouteIsRequired,
            tasks: composedTasks(tasks || (function () { return []; }), subroute),
            reducer: composedReducer(path, reducer ? reducer : function (r) { return r; }, subroute, subrouteIsRequired),
            accessLevel: composedAccessLevel(accessLevel || AccessLevel.PUBLIC, subroute),
            hideSidebar: composedPredicate(hideSidebar, 'hideSidebar', subroute),
            hideTopBar: composedPredicate(hideTopBar, 'hideTopBar', subroute),
            doNotResetWindowScroll: composedPredicate(doNotResetWindowScroll, 'doNotResetWindowScroll', subroute),
        });
    };
};
