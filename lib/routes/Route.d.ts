import { Location } from 'history';
import { Action as ReduxAction } from 'redux';
import { Task } from '../tasks/tasks';
import { AnyArray, Omit } from '../types';
import { Subroute } from './Subroute';
declare type Variables = {
    [name: string]: string | undefined;
};
declare type SubrouteOf<Route> = 'subroute' extends keyof Route ? Route extends {
    subroute?: infer Subroute;
} ? Subroute : undefined : undefined;
declare type SubrouteMetaOf<State, Action extends ReduxAction, Route> = SubrouteOf<Route> extends undefined ? undefined : SubrouteMeta<State, Action, SubrouteOf<Route>>;
declare type WithoutSubroute<Route> = Route extends {
    subroute: infer Subroute;
} ? Omit<Route, 'subroute'> : Route;
declare type WithOptionalSubroute<Route> = Route extends {
    subroute: infer Subroute;
} ? Omit<Route, 'subroute'> & {
    subroute?: Subroute;
} : Route;
declare type SubrouteIsRequired<Route> = Route extends {
    subroute: infer Subroute;
} ? (Subroute extends undefined ? undefined : true) : undefined;
declare type FromPath<Route> = (path: string, location: Location) => Route | undefined;
declare type ToPath<Route> = (route: Route) => string;
declare type Tasks<State, Action extends ReduxAction, Route> = (route: Route, state: State) => Task<Action>[];
declare type Reducer<State, Action extends ReduxAction, Route> = (route: Route, action: Action, state: State) => Route | undefined;
declare type GetAccessLevel<Route> = (route: Route) => AccessLevel;
declare type Predicate<Route> = (route: Route) => boolean;
export interface SubrouteMeta<State, Action extends ReduxAction, Route> {
    fromPath: FromPath<Route>;
    toPath: ToPath<Route>;
    tasks: Tasks<State, Action, Route>;
    reducer: Reducer<State, Action, Route>;
    accessLevel: GetAccessLevel<Route>;
    hideSidebar: Predicate<Route>;
    hideTopBar: Predicate<Route>;
    doNotResetWindowScroll: Predicate<Route>;
}
export interface RouteMeta<State, Action extends ReduxAction, Path extends string, Route, Args extends AnyArray> extends SubrouteMeta<State, Action, RouteType<Path, Route>> {
    (...args: Args): RouteType<Path, Route>;
    path: Path;
    subroute: SubrouteMetaOf<State, Action, Route>;
    subrouteIsRequired: SubrouteIsRequired<Route>;
}
declare type RouteType<Path extends string, Route> = {
    path: Path;
} & Route;
declare type AreSame<A, B> = A extends B ? (B extends A ? true : false) : false;
declare type IsPartial<A> = AreSame<A, Partial<A>>;
declare type DefaultArgs<T> = IsPartial<T> extends true ? [] : [T];
declare type RequiredFromPathOptions<Route> = {
    fromPath: (variables: Variables) => WithOptionalSubroute<Route> | undefined;
};
declare type FromPathOptions<Route> = IsPartial<WithoutSubroute<Route>> extends true ? Partial<RequiredFromPathOptions<Route>> : RequiredFromPathOptions<Route>;
declare type SubrouteOptions<State, Action extends ReduxAction, Route> = SubrouteMetaOf<State, Action, Route> extends undefined ? {
    subroute?: undefined;
} : {
    subroute: SubrouteMetaOf<State, Action, Route>;
};
declare type SubrouteIsRequiredOptions<Route> = AreSame<SubrouteIsRequired<Route>, true> extends true ? {
    subrouteIsRequired: true;
} : {
    subrouteIsRequired?: undefined;
};
declare type RouteOptions<State, Action extends ReduxAction, Path extends string, Route, Args extends AnyArray> = FromPathOptions<Route> & {
    path: Path;
    init?: (...args: Args) => Route;
    tasks?: Tasks<State, Action, Route>;
    reducer?: Reducer<State, Action, Route>;
    accessLevel?: AccessLevel;
    hideSidebar?: boolean;
    hideTopBar?: boolean;
    doNotResetWindowScroll?: boolean;
} & SubrouteOptions<State, Action, Route> & SubrouteIsRequiredOptions<Route>;
export declare enum AccessLevel {
    PUBLIC = 0,
    LOGGED_IN = 1,
    PRIVATE = 2
}
export declare const RouteConstructor: <State, Action extends ReduxAction<any>>() => <Path extends string, Content, Args extends any[] = DefaultArgs<Content>>({ path, init, fromPath, subroute, subrouteIsRequired, tasks, reducer, accessLevel, hideSidebar, hideTopBar, doNotResetWindowScroll, }: RouteOptions<State, Action, Path, Content, Args>) => RouteMeta<State, Action, Path, Content, Args>;
export declare type Route<Meta> = Meta extends RouteMeta<infer State, infer Action, infer Path, infer Route, infer Args> ? RouteType<Path, Route> : Subroute<Meta>;
export {};
