"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var compact_1 = require("../compact");
var Route_1 = require("./Route");
var withMeta = function (
// tslint:disable-next-line:no-any
metas, route, 
// tslint:disable-next-line:no-any
withMeta) {
    if (!route) {
        return undefined;
    }
    var meta = metas.find(function (meta) { return meta.path == route.path; });
    return meta && withMeta(meta);
};
exports.SubrouteConstructor = function () {
    // tslint:disable-next-line:no-any
    return function () {
        var metas = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            metas[_i] = arguments[_i];
        }
        return ({
            fromPath: function (path, location) {
                return compact_1.compact.apply(void 0, metas.map(function (meta) { return meta.fromPath(path, location); }))[0];
            },
            toPath: function (route) { return withMeta(metas, route, function (meta) { return meta.toPath(route); }) || ''; },
            tasks: function (route, state) {
                return withMeta(metas, route, function (meta) { return meta.tasks(route, state); }) || [];
            },
            reducer: function (route, action, state) {
                return withMeta(metas, route, function (meta) { return meta.reducer(route, action, state); });
            },
            accessLevel: function (route) {
                return withMeta(metas, route, function (meta) { return meta.accessLevel(route); }) ||
                    Route_1.AccessLevel.PUBLIC;
            },
            hideSidebar: function (route) {
                return withMeta(metas, route, function (meta) { return meta.hideSidebar(route); }) || false;
            },
            hideTopBar: function (route) {
                return withMeta(metas, route, function (meta) { return meta.hideTopBar(route); }) || false;
            },
            doNotResetWindowScroll: function (route) {
                return withMeta(metas, route, function (meta) { return meta.doNotResetWindowScroll(route); }) ||
                    false;
            },
        });
    };
};
