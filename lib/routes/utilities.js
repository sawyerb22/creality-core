"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable-next-line:no-any
exports.findRoute = function (route, subroute) {
    return route.path == subroute.path
        ? route
        : route.subroute && exports.findRoute(route.subroute, subroute);
};
