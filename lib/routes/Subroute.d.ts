import { Action as ReduxAction } from 'redux';
import { Route, RouteMeta, SubrouteMeta } from './Route';
declare type RouteTypes<T> = {
    [K in keyof T]: Route<T[K]>;
};
export declare const SubrouteConstructor: <State, Action extends ReduxAction<any>>() => <Metas extends RouteMeta<State, Action, any, any, any>[]>(...metas: Metas) => SubrouteMeta<State, Action, RouteTypes<Metas>[number]>;
export declare type Subroute<Meta> = Meta extends SubrouteMeta<infer State, infer Action, infer Route> ? Route : never;
export {};
