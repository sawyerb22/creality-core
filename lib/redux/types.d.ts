export declare type FunctionType = (...args: any[]) => any;
export declare type ArgumentTypes<T> = T extends (...args: infer U) => infer R ? U : never;
