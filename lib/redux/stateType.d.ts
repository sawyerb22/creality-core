import { FunctionType } from './types';
export declare type ReducerMap<T> = {
    [K in keyof T]: StateType<T[K]>;
};
export declare type StateType<ReducerOrMap> = ReducerOrMap extends FunctionType ? ReturnType<ReducerOrMap> : ReducerOrMap extends object ? ReducerMap<ReducerOrMap> : never;
