import { DispatchProp, InferableComponentEnhancer, InferableComponentEnhancerWithProps, MapDispatchToPropsParam, MapStateToPropsParam } from 'react-redux';
export interface Connect<State> {
    (): InferableComponentEnhancer<DispatchProp>;
    <TStateProps = {}, no_dispatch = {}, TOwnProps = {}>(mapStateToProps: MapStateToPropsParam<TStateProps, TOwnProps, State>): InferableComponentEnhancerWithProps<TStateProps & DispatchProp, TOwnProps>;
    <no_state = {}, TDispatchProps = {}, TOwnProps = {}>(mapStateToProps: null | undefined, mapDispatchToProps: MapDispatchToPropsParam<TDispatchProps, TOwnProps>): InferableComponentEnhancerWithProps<TDispatchProps, TOwnProps>;
    <TStateProps = {}, TDispatchProps = {}, TOwnProps = {}>(mapStateToProps: MapStateToPropsParam<TStateProps, TOwnProps, State>, mapDispatchToProps: MapDispatchToPropsParam<TDispatchProps, TOwnProps>): InferableComponentEnhancerWithProps<TStateProps & TDispatchProps, TOwnProps>;
}
