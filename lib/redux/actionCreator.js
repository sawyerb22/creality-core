"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActionCreator = function (type, actionCreator) {
    // tslint:disable-next-line:no-any
    var mutableCreator = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return Object.assign({}, { type: type }, actionCreator ? actionCreator.apply(void 0, args) : {});
    };
    mutableCreator.type = type;
    return mutableCreator;
};
