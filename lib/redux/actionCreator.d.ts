import { ArgumentTypes, FunctionType } from './types';
export interface ActionCreator<Type extends string, PayloadCreator extends FunctionType> {
    (...a: ArgumentTypes<PayloadCreator>): Action<Type, ReturnType<PayloadCreator>>;
    type: Type;
}
export declare const ActionCreator: <Type extends string, PayloadCreator extends FunctionType = () => {}>(type: Type, actionCreator?: PayloadCreator | undefined) => ActionCreator<Type, PayloadCreator>;
export declare type Action<StringType, Payload = {}> = {
    type: StringType;
} & Payload;
export declare type BoundActionCreator<T> = T extends ActionCreator<infer Type, infer PayloadCreator> ? (...a: ArgumentTypes<PayloadCreator>) => void : T extends FunctionType ? (...a: ArgumentTypes<T>) => void : never;
export declare type ActionCreatorMap<T> = {
    [K in keyof T]: ActionType<T[K]>;
};
export declare type ActionType<ActionCreatorOrMap extends any> = ActionCreatorOrMap extends ActionCreator<StringType, FunctionType> ? ReturnType<ActionCreatorOrMap> : ActionCreatorOrMap extends Record<any, any> ? {
    [K in keyof ActionCreatorOrMap]: ActionType<ActionCreatorOrMap[K]>;
}[keyof ActionCreatorOrMap] : ActionCreatorOrMap extends infer R ? never : never;
