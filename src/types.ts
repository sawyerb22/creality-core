export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>
// tslint:disable-next-line:no-any
export type AnyArray = any[]
