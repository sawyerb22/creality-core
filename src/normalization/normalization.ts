/* tslint:disable:no-any */
import deepmerge, { all } from 'deepmerge'
import mapValues from 'lodash.mapvalues'
import pickBy from 'lodash.pickby'
import { Action as ReduxAction } from 'redux'
import { isArray } from 'util'

const deepmergeOptions: deepmerge.Options = {
  arrayMerge: (destination, source) => source,
}

const normalizeObject = <T extends object>(object: T) =>
  mapValues(object, normalizeValue)

const normalizeValue = (value: any): any => {
  if (typeof value != 'object' || value == null) {
    return value
  } else if (isArray(value)) {
    return value.map(normalizeValue)
  } else if (isNormalizable(value)) {
    return createNormalize<any>()(value)
  } else {
    return normalizeObject(value)
  }
}

type NormalizedDataType = {
  [typename: string]: {
    [id: string]: any
  }
}

const denormalizeObject = <NormalizedData extends NormalizedDataType>(
  object: any,
  data: NormalizedData,
): any => mapValues(object, value => denormalizeValue(value, data))

const denormalizeValue = <NormalizedData extends NormalizedDataType>(
  value: any,
  data: NormalizedData,
): any => {
  if (typeof value != 'object' || value == null) {
    return value
  } else if (isArray(value)) {
    return value.map(x => denormalizeValue(x, data))
  } else if (isNormalized(value)) {
    return createDenormalize<NormalizedData>()(data)(value)
  } else {
    return denormalizeObject(value, data)
  }
}

export type Normalizable<
  NormalizedData extends NormalizedDataType,
  Key extends keyof NormalizedData
> = {
  __typename: Key
  id: string
}

const isNormalizable = (value: any): value is Normalizable<any, any> =>
  value && typeof value.__typename == 'string' && typeof value.id == 'string'

type KeyOf<T> = T extends Normalizable<any, infer Key> ? Key : never

const isNormalized = (value: any): value is Normalized<any, any> =>
  value &&
  typeof value.__typename == 'string' &&
  typeof value.id == 'string' &&
  value.isNormalized == true

const gatherNormalizedData = <NormalizedData extends NormalizedDataType>(
  value: any,
): Partial<NormalizedData> => {
  if (typeof value != 'object' || value == null) {
    return {}
  } else if (isArray(value)) {
    return all(value.map(gatherNormalizedData), deepmergeOptions)
  } else {
    return all(
      [
        isNormalizable(value)
          ? { [value.__typename]: { [value.id]: normalizeObject(value) } }
          : {},
        ...Object.values(value).map(gatherNormalizedData),
      ],
      deepmergeOptions,
    )
  }
}

export type Normalized<
  NormalizedData extends NormalizedDataType,
  T extends Normalizable<NormalizedData, any>
> = {
  __typename: KeyOf<T>
  id: string
  isNormalized: true
}

export const createNormalize = <
  NormalizedData extends NormalizedDataType
>() => <T extends Normalizable<NormalizedData, any>>(
  value: T,
): Normalized<NormalizedData, T> => ({
  __typename: value.__typename,
  id: value.id,
  isNormalized: true,
})

type NormalizedDataReducers<NormalizedData, Action> = {
  [K in keyof NormalizedData]?: ((
    data: NormalizedData[K],
    action: Action,
  ) => NormalizedData[K])
}

export const createReducer = <
  NormalizedData extends NormalizedDataType,
  Action extends ReduxAction
>(
  normalizedData: NormalizedData,
  reducers: NormalizedDataReducers<NormalizedData, Action> = {},
) => (
  data: NormalizedData = normalizedData,
  action: Action,
): NormalizedData => {
  const newData = gatherNormalizedData<NormalizedData>(action)
  return Object.keys(data).reduce(
    (data, k) => {
      const key = k as keyof NormalizedData
      const reducer = reducers[key]
      const nextSubset = reducer
        ? (reducer as any)(data[key], action)
        : data[key as keyof NormalizedData]
      return nextSubset !== data[key] ? { ...data, [key]: nextSubset } : data
    },
    Object.keys(newData).length > 0
      ? deepmerge(data, newData, deepmergeOptions)
      : data,
  )
}

type ById<T> = { [id: string]: T | undefined }

export const updateData = <T>(
  data: ById<T>,
  id: string,
  update: (value: T) => Partial<T>,
): ById<T> => {
  const value = data[id]
  return value
    ? {
        ...data,
        [id]: deepmerge(
          value,
          pickBy(update(value), value => value != undefined),
          deepmergeOptions,
        ) as T,
      }
    : data
}

export const createDenormalize: <
  NormalizedData extends NormalizedDataType
>() => <T extends Normalizable<any, any>>(
  data: NormalizedData,
) => (
  normalized: Normalized<NormalizedData, T>,
) => T = () => data => normalized =>
  denormalizeObject(data[normalized.__typename][normalized.id], data)
