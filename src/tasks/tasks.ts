import axios, { CancelTokenSource as RunningTask } from 'axios'
import { Action as ReduxAction, Dispatch, Store as ReduxStore } from 'redux'
import { Result } from '../result'
import { makeApiRequest } from './api'

export type Operation<Data> = { query: string; variables?: object }

export type Task<Action extends ReduxAction> = {
  operation: Operation<object>
  // tslint:disable-next-line:no-any
  completion: (result: Result<any>) => Action
}

export const TaskConstructor = <Action extends ReduxAction>() => <Data>(
  operation: Operation<Data>,
  completion: (result: Result<Data>) => Action,
): Task<Action> => ({
  operation,
  completion,
})

type KeyedTask<Action extends ReduxAction> = {
  name: string
  key: string
  start: (dispatch: Dispatch<Action>) => RunningTask
}

const operationName = (operationString: string): string => {
  const regex = /(query|mutation) (\w+)/.exec(operationString)
  return regex ? `${regex[1]} ${regex[2]}` : ''
}

const KeyedTask = <Action extends ReduxAction>({
  operation,
  completion,
}: Task<Action>): KeyedTask<Action> => ({
  name: operationName(operation.query),
  key: JSON.stringify(operation),
  start: dispatch => {
    const cancelToken = axios.CancelToken.source()
    makeApiRequest(
      axios.post('/graphql', operation, {
        cancelToken: cancelToken.token,
      }),
      data => dispatch(completion(Result(data)) as Action),
      error => dispatch(completion(Result(error)) as Action),
    )
    return cancelToken
  },
})

const mutableRunningTasks: { [key: string]: RunningTask } = {}

const logTasks = (tasks: KeyedTask<ReduxAction>[]) => {
  console.clear()
  tasks.forEach(task => console.log(task.name))
}

export const updateTasks = <State, Action extends ReduxAction>(
  store: ReduxStore<State, Action>,
  getTasks: (state: State) => Task<Action>[],
  maximumConcurrentTasks: number,
  enableLogging?: boolean,
) => {
  const keys = new Set<string>()
  const tasks = getTasks(store.getState())
    .map(task => KeyedTask<Action>(task))
    .filter(task => {
      // filter out duplicate tasks
      if (keys.has(task.key)) {
        return false
      } else {
        keys.add(task.key)
        return true
      }
    })
    .slice(0, maximumConcurrentTasks) // only keep up to our max

  if (enableLogging) {
    logTasks(tasks)
  }

  // Remove tasks no longer running
  Object.keys(mutableRunningTasks).forEach(key => {
    if (!keys.has(key)) {
      const cancelToken = mutableRunningTasks[key]
      cancelToken.cancel()
      delete mutableRunningTasks[key]
    }
  })

  // Start tasks not yet running
  tasks.forEach(task => {
    if (!(task.key in mutableRunningTasks)) {
      mutableRunningTasks[task.key] = task.start(store.dispatch)
    }
  })
}
