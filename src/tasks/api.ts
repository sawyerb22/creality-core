import axios, { AxiosPromise } from 'axios'

export const makeApiRequest = (
  promise: AxiosPromise,
  // tslint:disable-next-line:no-any
  handleSuccess: (data: any) => void,
  handleError: (error: Error) => void,
): Promise<void> =>
  promise
    .then(response => {
      const data = response && response.data
      if (data && data.errors) {
        handleError(Error('API Error: ' + data.errors[0].message))
        return
      }
      return handleSuccess(data && data.data ? data.data : data)
    })
    .catch(error => {
      if (error && error.errors) {
        handleError(Error('Request Error: ' + error.errors[0].message))
      } else if (error && error.message === 'Network Error') {
        handleError(error)
      } else if (axios.isCancel(error)) {
        return
      } else if (error) {
        handleError(error)
      }
    })
