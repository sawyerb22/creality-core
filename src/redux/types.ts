// tslint:disable-next-line:no-any
export type FunctionType = (...args: any[]) => any

export type ArgumentTypes<T> = T extends (...args: infer U) => infer R
  ? U
  : never
