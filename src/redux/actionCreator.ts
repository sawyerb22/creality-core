import { ArgumentTypes, FunctionType } from './types'

export interface ActionCreator<
  Type extends string,
  PayloadCreator extends FunctionType
> {
  (...a: ArgumentTypes<PayloadCreator>): Action<
    Type,
    ReturnType<PayloadCreator>
  >
  type: Type
}

export const ActionCreator = <
  Type extends string,
  PayloadCreator extends FunctionType = () => {}
>(
  type: Type,
  actionCreator?: PayloadCreator,
): ActionCreator<Type, PayloadCreator> => {
  // tslint:disable-next-line:no-any
  const mutableCreator = (...args: any[]) =>
    Object.assign({}, { type }, actionCreator ? actionCreator(...args) : {})
  mutableCreator.type = type
  return (mutableCreator as unknown) as ActionCreator<Type, PayloadCreator>
}

export type Action<StringType, Payload = {}> = {
  type: StringType
} & Payload

export type BoundActionCreator<T> = T extends ActionCreator<
  infer Type,
  infer PayloadCreator
>
  ? (...a: ArgumentTypes<PayloadCreator>) => void
  : T extends FunctionType
  ? (...a: ArgumentTypes<T>) => void
  : never

export type ActionCreatorMap<T> = { [K in keyof T]: ActionType<T[K]> }

export type ActionType<
  // tslint:disable-next-line:no-any
  ActionCreatorOrMap extends any
> = ActionCreatorOrMap extends ActionCreator<StringType, FunctionType>
  ? ReturnType<ActionCreatorOrMap> // tslint:disable-next-line:no-any
  : ActionCreatorOrMap extends Record<any, any>
  ? {
      [K in keyof ActionCreatorOrMap]: ActionType<ActionCreatorOrMap[K]>
    }[keyof ActionCreatorOrMap]
  : ActionCreatorOrMap extends infer R // should be just never but compiler yell with circularly references itself error
  ? never
  : never
