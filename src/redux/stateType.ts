import { FunctionType } from './types'

export type ReducerMap<T> = { [K in keyof T]: StateType<T[K]> }

export type StateType<ReducerOrMap> = ReducerOrMap extends FunctionType
  ? ReturnType<ReducerOrMap>
  : ReducerOrMap extends object
  ? ReducerMap<ReducerOrMap>
  : never
