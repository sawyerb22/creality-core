import { Location } from 'history'
import get from 'lodash.get'
import zip from 'lodash.zip'
import { Action as ReduxAction } from 'redux'
import { compact } from '../compact'
import { Task } from '../tasks/tasks'
import { AnyArray, Omit } from '../types'
import { Subroute } from './Subroute'

type Variables = {
  [name: string]: string | undefined
}

type SubrouteOf<Route> = 'subroute' extends keyof Route
  ? Route extends { subroute?: infer Subroute }
    ? Subroute
    : undefined
  : undefined

type SubrouteMetaOf<State, Action extends ReduxAction, Route> = SubrouteOf<
  Route
> extends undefined
  ? undefined
  : SubrouteMeta<State, Action, SubrouteOf<Route>>

type WithoutSubroute<Route> = Route extends { subroute: infer Subroute }
  ? Omit<Route, 'subroute'>
  : Route

type WithOptionalSubroute<Route> = Route extends { subroute: infer Subroute }
  ? Omit<Route, 'subroute'> & { subroute?: Subroute }
  : Route

type SubrouteIsRequired<Route> = Route extends { subroute: infer Subroute }
  ? (Subroute extends undefined ? undefined : true)
  : undefined

type FromPath<Route> = (path: string, location: Location) => Route | undefined

type ToPath<Route> = (route: Route) => string

type ToPathComponents<Route> = (route: Route) => string[]

type Tasks<State, Action extends ReduxAction, Route> = (
  route: Route,
  state: State,
) => Task<Action>[]

type Reducer<State, Action extends ReduxAction, Route> = (
  route: Route,
  action: Action,
  state: State,
) => Route | undefined

type GetAccessLevel<Route> = (route: Route) => AccessLevel

type Predicate<Route> = (route: Route) => boolean

export interface SubrouteMeta<State, Action extends ReduxAction, Route> {
  fromPath: FromPath<Route>
  toPath: ToPath<Route>
  tasks: Tasks<State, Action, Route>
  reducer: Reducer<State, Action, Route>
  accessLevel: GetAccessLevel<Route>
  hideSidebar: Predicate<Route>
  hideTopBar: Predicate<Route>
  doNotResetWindowScroll: Predicate<Route>
}

export interface RouteMeta<
  State,
  Action extends ReduxAction,
  Path extends string,
  Route,
  Args extends AnyArray
> extends SubrouteMeta<State, Action, RouteType<Path, Route>> {
  (...args: Args): RouteType<Path, Route>
  path: Path
  subroute: SubrouteMetaOf<State, Action, Route>
  subrouteIsRequired: SubrouteIsRequired<Route>
}

type RouteType<Path extends string, Route> = { path: Path } & Route

type IncrementalFromPath<Route> = (
  path: string,
  location: Location,
) => [WithOptionalSubroute<Route>, string] | undefined

type AreSame<A, B> = A extends B ? (B extends A ? true : false) : false

type IsPartial<A> = AreSame<A, Partial<A>>

type DefaultArgs<T> = IsPartial<T> extends true ? [] : [T]

type RequiredFromPathOptions<Route> = {
  fromPath: (variables: Variables) => WithOptionalSubroute<Route> | undefined
}

type FromPathOptions<Route> = IsPartial<WithoutSubroute<Route>> extends true
  ? Partial<RequiredFromPathOptions<Route>>
  : RequiredFromPathOptions<Route>

type SubrouteOptions<State, Action extends ReduxAction, Route> = SubrouteMetaOf<
  State,
  Action,
  Route
> extends undefined
  ? { subroute?: undefined }
  : { subroute: SubrouteMetaOf<State, Action, Route> }

type SubrouteIsRequiredOptions<Route> = AreSame<
  SubrouteIsRequired<Route>,
  true
> extends true
  ? { subrouteIsRequired: true }
  : { subrouteIsRequired?: undefined }

type RouteOptions<
  State,
  Action extends ReduxAction,
  Path extends string,
  Route,
  Args extends AnyArray
> = FromPathOptions<Route> & {
  path: Path
  init?: (...args: Args) => Route
  tasks?: Tasks<State, Action, Route>
  reducer?: Reducer<State, Action, Route>
  accessLevel?: AccessLevel
  hideSidebar?: boolean
  hideTopBar?: boolean
  doNotResetWindowScroll?: boolean
} & SubrouteOptions<State, Action, Route> &
  SubrouteIsRequiredOptions<Route>

const subrouteOf = <Route>(route: Route): SubrouteOf<Route> | undefined =>
  get(route, 'subroute')

const composedFromPath = <
  State,
  Action extends ReduxAction,
  Path extends string,
  Route
>(
  pattern: Path,
  fromPath: IncrementalFromPath<Route>,
  subroute?: SubrouteMetaOf<State, Action, Route>,
  subrouteIsRequired?: boolean,
): FromPath<RouteType<Path, Route>> => (path, location) => {
  const result = fromPath(path, location)
  if (!result) {
    return undefined
  }
  const [partialRouteWithoutPath, remainingPath] = result
  const partialRoute = Object.assign({}, partialRouteWithoutPath, {
    path: pattern,
  })
  if (subroute) {
    const possibleSubroute = subroute.fromPath(remainingPath, location)
    if (possibleSubroute) {
      return Object.assign({}, partialRoute, {
        subroute: possibleSubroute,
      }) as RouteType<Path, Route>
    }
  }
  return subrouteIsRequired && !get(partialRoute, 'subroute')
    ? undefined
    : (partialRoute as RouteType<Path, Route>)
}

const components = (path: string) =>
  path.split('/').filter(component => component != '')

const pathFromComponents = (components: string[]) => `/${components.join('/')}`

const remainingPath = (path: string[], pattern: string[]) =>
  pathFromComponents(path.slice(pattern.length))

const variables = (path: string[], pattern: string[]): Variables | undefined =>
  zip(path, pattern).reduce<Variables | undefined>(
    (variables, [path, pattern]) =>
      variables && pattern
        ? pattern.startsWith(':')
          ? { ...variables, [pattern.slice(1)]: path }
          : pattern === path
          ? variables
          : undefined
        : variables,
    {},
  )

const variablesAndRemainingPath = (
  path: string[],
  pattern: string[],
): [Variables | undefined, string] => [
  variables(path, pattern),
  remainingPath(path, pattern),
]

const fromPathWithPattern = <Route>(
  pattern: string,
  init: (variables: Variables) => WithOptionalSubroute<Route> | undefined,
): IncrementalFromPath<Route> => (path: string, location: Location) => {
  const [pathPattern, searchPattern] = pattern.split('?')
  const [variables, remainingPath] = variablesAndRemainingPath(
    components(path),
    components(pathPattern),
  )
  const result = variables && init(variables)
  return result ? [result, remainingPath] : undefined
}

const toPathComponentsWithPattern = <Route>(
  pattern: string,
): ToPathComponents<Route> => route =>
  compact(
    ...components(pattern).map(component =>
      component.startsWith(':') ? get(route, component.slice(1)) : component,
    ),
  )

const composedPath = <State, Action extends ReduxAction, Route>(
  pathComponents: (route: Route) => string[],
  subrouteMeta?: SubrouteMetaOf<State, Action, Route>,
): ToPath<Route> => route =>
  pathFromComponents([
    ...pathComponents(route),
    ...components(
      ifBoth(route, subrouteMeta, (meta, subroute) => meta.toPath(subroute)) ||
        '',
    ),
  ])

const composedTasks = <State, Action extends ReduxAction, Route>(
  tasks: Tasks<State, Action, Route>,
  subrouteMeta?: SubrouteMetaOf<State, Action, Route>,
): Tasks<State, Action, Route> => (route, state) => [
  ...(ifBoth(route, subrouteMeta, (meta, subroute) =>
    meta.tasks(subroute, state),
  ) || []),
  ...tasks(route, state),
]

const composedReducer = <
  State,
  Action extends ReduxAction,
  Path extends string,
  Route
>(
  path: Path,
  reducer: Reducer<State, Action, Route>,
  subrouteMeta?: SubrouteMetaOf<State, Action, Route>,
  subrouteIsRequired?: boolean,
): Reducer<State, Action, RouteType<Path, Route>> => (route, action, state) => {
  const newRoute = reducer(route, action, state)
  if (newRoute === undefined) {
    return undefined
  }
  const subroute = subrouteOf(newRoute)
  if (subrouteMeta && subroute) {
    const newSubroute = subrouteMeta.reducer(subroute, action, state)
    if (newSubroute !== subroute) {
      return newSubroute === undefined && subrouteIsRequired
        ? undefined
        : Object.assign({}, newRoute, { subroute: newSubroute }, { path })
    }
  }
  return get(newRoute, 'path') === path
    ? (newRoute as RouteType<Path, Route>)
    : Object.assign({}, newRoute, { path })
}

export enum AccessLevel {
  PUBLIC = 0,
  LOGGED_IN = 1,
  PRIVATE = 2,
}

const composedAccessLevel = <State, Action extends ReduxAction, Route>(
  accessLevel: AccessLevel,
  subrouteMeta?: SubrouteMetaOf<State, Action, Route>,
): GetAccessLevel<Route> => route =>
  Math.max(
    accessLevel,
    ifBoth(route, subrouteMeta, (meta, subroute) =>
      meta.accessLevel(subroute),
    ) || 0,
  )

type Predicates<State, Action extends ReduxAction, Route> = {
  [K in keyof SubrouteMeta<State, Action, Route>]: SubrouteMeta<
    State,
    Action,
    Route
  >[K] extends Predicate<Route>
    ? K
    : never
}[keyof SubrouteMeta<State, Action, Route>]

const ifBoth = <State, Action extends ReduxAction, Route, T>(
  route: Route,
  subrouteMeta: SubrouteMetaOf<State, Action, Route> | undefined,
  withSubroute: (
    subrouteMeta: SubrouteMeta<State, Action, SubrouteOf<Route>>,
    subroute: SubrouteOf<Route>,
  ) => T,
) => {
  const subroute = subrouteOf(route)
  return subrouteMeta && subroute
    ? withSubroute(
        subrouteMeta as SubrouteMeta<State, Action, SubrouteOf<Route>>,
        subroute,
      )
    : undefined
}

const composedPredicate = <State, Action extends ReduxAction, Route>(
  value: boolean | undefined,
  predicate: Predicates<State, Action, Route>,
  subrouteMeta?: SubrouteMetaOf<State, Action, Route>,
): Predicate<Route> => route =>
  value ||
  ifBoth(route, subrouteMeta, (meta, subroute) => meta[predicate](subroute)) ||
  false

export const RouteConstructor = <State, Action extends ReduxAction>() =>
  /**
   * Creates a Route definition
   * @param path
   * REQUIRED: A pattern like `'/contacts/:id'`
   * @param init
   * A initializer that returns the route body
   * @param fromPath
   * An initializer that returns the route body given matching path variables
   * @param subroute
   * The Subroute definition if the route body contains a subroute
   * @param subrouteIsRequired
   * Must be marked true if `subroute` is not optional
   * @param tasks
   * A method that returns the tasks for this route
   * @param reducer
   * A reducer for this route
   * @param accessLevel
   * The access level for this route
   * @param hideSidebar
   * Whether to hide the sidebar when viewing this route.
   * @param hideTopBar
   * Whether to hide the top bar when viewing this route.
   * @param doNotResetWindowScroll
   * Mark `true` if the window scroll should not reset when navigating to or from this route.
   */
  <Path extends string, Content, Args extends AnyArray = DefaultArgs<Content>>({
    path,
    init,
    fromPath,
    subroute,
    subrouteIsRequired,
    tasks,
    reducer,
    accessLevel,
    hideSidebar,
    hideTopBar,
    doNotResetWindowScroll,
  }: RouteOptions<State, Action, Path, Content, Args>): RouteMeta<
    State,
    Action,
    Path,
    Content,
    Args
  > =>
    Object.assign(
      (...args: Args) =>
        Object.assign({}, init ? init(...args) : args[0] || {}, { path }),
      {
        path,
        fromPath: composedFromPath(
          path,
          fromPathWithPattern(
            path,
            fromPath || (() => ({} as WithOptionalSubroute<Content>)),
          ),
          subroute,
          subrouteIsRequired,
        ),
        toPath: composedPath(toPathComponentsWithPattern(path), subroute),
        subroute: subroute as SubrouteMetaOf<State, Action, Content>,
        subrouteIsRequired: subrouteIsRequired as SubrouteIsRequired<Content>,
        tasks: composedTasks(tasks || (() => []), subroute),
        reducer: composedReducer(
          path,
          reducer ? reducer : r => r,
          subroute,
          subrouteIsRequired,
        ),
        accessLevel: composedAccessLevel(
          accessLevel || AccessLevel.PUBLIC,
          subroute,
        ),
        hideSidebar: composedPredicate(hideSidebar, 'hideSidebar', subroute),
        hideTopBar: composedPredicate(hideTopBar, 'hideTopBar', subroute),
        doNotResetWindowScroll: composedPredicate(
          doNotResetWindowScroll,
          'doNotResetWindowScroll',
          subroute,
        ),
      },
    )

export type Route<Meta> = Meta extends RouteMeta<
  infer State,
  infer Action,
  infer Path,
  infer Route,
  infer Args
>
  ? RouteType<Path, Route>
  : Subroute<Meta>
