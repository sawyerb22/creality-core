import { Route, RouteMeta } from './Route'

export type RouteType = {
  path: string
  subroute?: RouteType
}

// tslint:disable-next-line:no-any
export const findRoute = <Meta extends RouteMeta<any, any, any, any, any>>(
  route: RouteType,
  subroute: Meta,
): Route<Meta> | undefined =>
  route.path == subroute.path
    ? ((route as unknown) as Route<Meta>)
    : route.subroute && findRoute(route.subroute, subroute)
