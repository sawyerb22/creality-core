import { Action as ReduxAction } from 'redux'
import { compact } from '../compact'
import { AnyArray } from '../types'
import { AccessLevel, Route, RouteMeta, SubrouteMeta } from './Route'

type RouteTypes<T> = { [K in keyof T]: Route<T[K]> }
type Unionize<T extends AnyArray, U = never> = T[number] | U

const withMeta = <State, Action extends ReduxAction, T>(
  // tslint:disable-next-line:no-any
  metas: RouteMeta<State, Action, any, any, any>[],
  route: { path: string } | undefined,
  // tslint:disable-next-line:no-any
  withMeta: (meta: RouteMeta<State, Action, any, any, any>) => T,
): T | undefined => {
  if (!route) {
    return undefined
  }
  const meta = metas.find(meta => meta.path == route.path)
  return meta && withMeta(meta)
}

export const SubrouteConstructor = <State, Action extends ReduxAction>() =>
  // tslint:disable-next-line:no-any
  <Metas extends RouteMeta<State, Action, any, any, any>[]>(
    ...metas: Metas
  ): SubrouteMeta<State, Action, Unionize<RouteTypes<Metas>>> => ({
    fromPath: (path, location) =>
      compact(...metas.map(meta => meta.fromPath(path, location)))[0],
    toPath: route => withMeta(metas, route, meta => meta.toPath(route)) || '',
    tasks: (route, state) =>
      withMeta(metas, route, meta => meta.tasks(route, state)) || [],
    reducer: (route, action, state) =>
      withMeta(metas, route, meta => meta.reducer(route, action, state)),
    accessLevel: route =>
      withMeta(metas, route, meta => meta.accessLevel(route)) ||
      AccessLevel.PUBLIC,
    hideSidebar: route =>
      withMeta(metas, route, meta => meta.hideSidebar(route)) || false,
    hideTopBar: route =>
      withMeta(metas, route, meta => meta.hideTopBar(route)) || false,
    doNotResetWindowScroll: route =>
      withMeta(metas, route, meta => meta.doNotResetWindowScroll(route)) ||
      false,
  })

export type Subroute<Meta> = Meta extends SubrouteMeta<
  infer State,
  infer Action,
  infer Route
>
  ? Route
  : never
