export type ValueOrError<T> =
  | {
      type: typeof RESULT
      value: T
      error?: undefined
    }
  | {
      type: typeof RESULT
      value?: undefined
      error: Error
    }

// tslint:disable-next-line:no-any
export const isValueOrError = <T>(arg: any): arg is ValueOrError<T> =>
  arg && ((arg.value && !arg.error) || (!arg.value && arg.error))

const RESULT = 'RESULT'

export type NotNullOrUndefined<T> = Exclude<T, undefined | null>

export type Result<T> = ValueOrError<T> & {
  type: typeof RESULT
  map<U>(transform: (value: T) => U): Result<U>
  match<U>(success: (value: T) => U, failure: (error: Error) => U): U
  get<K extends keyof T>(key: K): T[K] | undefined
  select<K extends keyof T>(key: K): Result<NotNullOrUndefined<T[K]>>
}

export const Result = <T>(valueOrError: T | Error): Result<T> =>
  valueOrError instanceof Error
    ? failureResult(valueOrError)
    : successResult(valueOrError)

const successResult = <T>(value: T): Result<T> => ({
  value,
  type: RESULT,
  map: <U>(transform: (value: T) => U): Result<U> => {
    try {
      return successResult(transform(value))
    } catch (error) {
      return failureResult(error)
    }
  },
  match: <U>(success: (value: T) => U, failure: (error: Error) => U) => {
    try {
      return success(value)
    } catch (error) {
      return failure(error)
    }
  },
  get: key => value[key],
  select: <K extends keyof T>(key: K): Result<NotNullOrUndefined<T[K]>> => {
    const selected = value[key]
    return selected === null
      ? failureResult(Error(`${key} is null on ${value}`))
      : selected === undefined
      ? failureResult(Error(`${key} is undefined on ${value}`))
      : successResult<NotNullOrUndefined<T[K]>>(selected as NotNullOrUndefined<
          T[K]
        >)
  },
})

const failureResult = <T>(error: Error): Result<T> => ({
  error,
  type: RESULT,
  map: () => failureResult(error),
  match: <U>(_: (value: T) => U, failure: (error: Error) => U) =>
    failure(error),
  get: () => undefined,
  select: key => failureResult(error),
})

export const zipResults = <V>(
  results: { [K in keyof V]: Result<V[K]> },
): Result<V> =>
  Result(
    Object.keys(results).reduce(
      (valueOrError: V | Error, key) =>
        valueOrError instanceof Error
          ? valueOrError
          : results[key as keyof V].value
          ? { ...valueOrError, [key]: results[key as keyof V].value }
          : (results[key as keyof V].error as Error),
      {} as V,
    ),
  )

// tslint:disable-next-line:no-any
export const isResult = <T>(arg: any): arg is Result<T> =>
  isValueOrError(arg) && arg.type === RESULT

export default Result
