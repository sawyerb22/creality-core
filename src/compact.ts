export const compact = <T>(
  ...elements: (T | undefined | null | false)[]
): T[] => elements.filter(x => x) as T[]

export default compact
